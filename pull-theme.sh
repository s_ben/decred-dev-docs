#!/bin/bash

# If the initial repo does not contain the subtree we add it here.
if [[ ! -d themes/gohugoioTheme/ ]]; then
  git subtree add --prefix=themes/gohugoioTheme/ --squash git@github.com:gohugoio/gohugoioTheme.git master
else
  git subtree pull --prefix=themes/gohugoioTheme/ git@github.com:gohugoio/gohugoioTheme.git master --squash
fi


