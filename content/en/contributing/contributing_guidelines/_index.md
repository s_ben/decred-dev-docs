---
title: "Contributing_guidelines"
linktitle: "Contributing Guidelines"
description: Some Description
date: 2018-10-22T14:03:14-07:00
publishdate: 2018-10-22T14:03:14-07:00
lastmod: 2018-10-22T14:03:14-07:00
categories: []
keywords: []
menu:
  docs:
    parent: "contributing"
    weight: 1
weight: 1
draft: false
aliases: [/Contributing_guidelines/]
toc: false
---

While the process of contributing to Decred will vary somewhat from project to project, the basic process is as follows:

1)	Find a project you have the skills to contribute to. See [Picking a Project] for inspiration. Generally, we find that contributors do their best work when working on projects they’re passionate about. 

2) 	Browse the project’s open Issues on GitHub for an Issue you think you can handle. You may also submit a new Issue proposing a feature, enhancement or bug fix. Before submitting a new Issue, be sure to search the existing open Issues to make sure you’re not duplicating work. 

3) 	Announce your plans to other developers before you start work. This helps avoid duplicate work, minimizes the chance you waste time and energy working on something that doesn’t fit with the project’s plan or architecture, and permits discussion that can help you achieve your goals. 

4) 	Fork the project repo, make your changes in a local branch, and submit a Pull Request (PR) for review. 

Some projects will have more detailed instructions on contributing (e.g. PR etiquette, custom git flows, preferred methods of communication, etc.), which can typically be found in the README on a project’s GitHub repo. 

For more detailed instructions on using git and GitHub, see [Using GitHub]
