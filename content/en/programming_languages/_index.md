---
title: "Programming languages"
linktitle: "Programming languages"
description: Some Description
date: 2018-08-16T11:56:25-07:00
publishdate: 2018-08-16T11:56:25-07:00
lastmod: 2018-08-16T11:56:25-07:00
categories: []
keywords: []
menu:
  docs:
    parent: "Programming languages"
    weight: 2
weight: 1
draft: false
aliases: [/Programming_languages/]
toc: false
---

## Languages Used

- Go
- Frontend Javascript
- Node.js
- Rust

## Frameworks Used

- React
- redux
