---
title: Environments
linktitle: Environments
description: How to setup your environment
date: 2018-08-13
publishdate: 2018-08-13
lastmod: 2018-08-13
categories: []
keywords: []
menu:
  docs:
    parent: "Environments"
    weight: 1
weight: 1
draft: false
aliases: [/Environment/]
toc: false
---

When working on any project keep in mind there are several global development environments to work with. These environments are shared among all developers and users.

You will need to choose an environment to working depending on the needs of your tests and code.

## Mainnet

Consider Mainnet to be production. If you mess something up here there are consequences. Also remember the blockchain is immutable, so there are no rollbacks of transactions. You can roll forward with hardforks but that usually causes community outcries.

## Testnet

When testing out new features in a "production" like environment this is what you should use.

## Localized testnets

These are environments that are spun up for the duration of the feature that only the developer(s) use and are destroyed after a feature is merged.

## Simnet
